export class DestinoViaje{
   private selected:boolean;
    id :number;
    public servicios: string[];
    constructor(public nombre:string,public u:string, public vote:number = 0){//N = nombre y u = urlImagen
       this.selected =  false;
       this.servicios = ['pileta','desayuno'];
       this.id =0;
    }

    isSelected():boolean{
        return this.selected;
    }

    setSelected(s:boolean){
        this.selected=s;
    }

    voteUp(){
        this.vote ++;
    }

    voteDown(){
        this.vote --;
    }



}